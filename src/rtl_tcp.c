/*
 * rtl_tcp - I/Q spectrum server for RTL2832 based DVB-T receivers
 *
 * Copyright (C) 2012 by Steve Markgraf <steve@steve-m.de>
 * Copyright (C) 2012-2013 by Hoernchen <la@tfc-server.de>
 * Copyright (C) 2013 by Roman Kraevskiy <rkraevskiy@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/* #define DEBUG */
/* #define USE_SPINLOCK */
#define NDROP 1


#include <errno.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef _WIN32
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <netinet/tcp.h>
#include <net/if.h>
#else
#include <winsock2.h>
#include "getopt/getopt.h"
#endif

#include <pthread.h>

#include "rtl-sdr.h"
#include "convenience/convenience.h"

#ifdef _WIN32
#pragma comment(lib, "ws2_32.lib")

typedef int socklen_t;

#else
#define closesocket close
#define SOCKADDR struct sockaddr
#define SOCKET int
#define SOCKET_ERROR -1
#endif

static SOCKET s;

static pthread_t tcp_worker_thread;
static pthread_t command_thread;

static pthread_mutex_t ll_mutex;
#ifdef USE_SPINLOCK
static pthread_spinlock_t ll_spinlock;
#endif
static pthread_cond_t cond;

struct llist {
    struct llist *next;
    struct llist *prev;
	size_t len;
    char data[1];
};

typedef struct { /* structure size must be multiple of 2 bytes */
	char magic[4];
	uint32_t tuner_type;
	uint32_t tuner_gain_count;
} dongle_info_t;

static rtlsdr_dev_t *dev = NULL;

static int global_numq = 0;
static struct llist *ll_buffers_head;
static struct llist *ll_buffers_tail;

volatile int ll_nqueued = 0;
static int llbuf_num=500;

static volatile int do_exit = 0;
static volatile int thread_exit = 0;
static int buf_len;

void usage(void)
{
    printf("rtl_tcp, an I/Q spectrum server for RTL2832 based DVB-T receivers\n\n"
		"Usage:\t[-a listen address]\n"
		"\t[-p listen port (default: 1234)]\n"
		"\t[-f frequency to tune to [Hz]]\n"
		"\t[-g gain (default: 0 for auto)]\n"
		"\t[-s samplerate in Hz (default: 2048000 Hz)]\n"
		"\t[-b number of buffers (default: 32, set by library)]\n"
		"\t[-n max number of linked list buffers to keep (default: 500)]\n"
		"\t[-l read buffer size, kbytes (default: library default)]\n"
		"\t[-d device index (default: 0)]\n"
		"\t[-P ppm_error (default: 0)]\n");
	exit(1);
}


void exit_threads(){
    if (!thread_exit){
        thread_exit = 1;
        rtlsdr_cancel_async(dev);
		pthread_mutex_lock(&ll_mutex);
		pthread_cond_signal(&cond);
		pthread_mutex_unlock(&ll_mutex);
    }
}


#ifdef _WIN32
int gettimeofday(struct timeval *tv, void* ignored)
{
	FILETIME ft;
	unsigned __int64 tmp = 0;
	if (NULL != tv) {
		GetSystemTimeAsFileTime(&ft);
		tmp |= ft.dwHighDateTime;
		tmp <<= 32;
		tmp |= ft.dwLowDateTime;
		tmp /= 10;
#ifdef _MSC_VER
		tmp -= 11644473600000000Ui64;
#else
		tmp -= 11644473600000000ULL;
#endif
		tv->tv_sec = (long)(tmp / 1000000UL);
		tv->tv_usec = (long)(tmp % 1000000UL);
	}
	return 0;
}

BOOL WINAPI
sighandler(int signum)
{
	if (CTRL_C_EVENT == signum) {
		fprintf(stderr, "Signal caught, exiting!\n");
		do_exit = 1;
		return TRUE;
	}
	return FALSE;
}
#else
static void sighandler(int signum)
{
    signum = signum;

	fprintf(stderr, "Signal caught, exiting!\n");
	if (!do_exit) {
      do_exit = 1;
    }
}
#endif

void llpush(struct llist *rpt)
{
    rpt->prev = ll_buffers_tail;
    rpt->next = NULL;

    if (ll_buffers_tail){
        ll_buffers_tail->next = rpt;
    }else{
        ll_buffers_head = rpt;
    }
    ll_buffers_tail = rpt;
    ll_nqueued++;
}
   
struct llist *llpop()
{
    struct llist *ret = NULL;

    if (ll_nqueued){
        ll_nqueued--;
        ret = ll_buffers_head;
        if (ret->next){
            ret->next->prev = NULL;
        }else{
            ll_buffers_tail = NULL;
        }
        ll_buffers_head = ret->next;
    }
    return ret;
}
   
struct llist *llpopex()
{
    struct llist *ret = NULL;

    if (ll_nqueued){
        ll_nqueued = 0;
        ret = ll_buffers_head;
        ll_buffers_head = ll_buffers_tail = NULL;
    }
    return ret;
}
   

static void llclear()
{
#ifdef USE_SPINLOCK
        pthread_spin_lock(&ll_spinlock);
#else
        pthread_mutex_lock(&ll_mutex);
#endif
		while(ll_buffers_head) {
			free(llpop());
		}
#ifdef USE_SPINLOCK
        pthread_spin_unlock(&ll_spinlock);
#else
        pthread_mutex_unlock(&ll_mutex);
#endif
}

void rtlsdr_callback(unsigned char *buf, uint32_t len, void *ctx)
{
    if(!thread_exit) {
        struct llist *rpt;
        int num_queued;
        int dropped = 0;
        struct llist *to_free[NDROP];

        ctx = ctx;
        
        rpt = (struct llist*)malloc(sizeof(struct llist)+len);
        memcpy(rpt->data, buf, len);
        rpt->len = len;

#ifdef USE_SPINLOCK
        pthread_spin_lock(&ll_spinlock);
#else
        pthread_mutex_lock(&ll_mutex);
#endif
        llpush(rpt);
        if(llbuf_num && llbuf_num < ll_nqueued){
            for(dropped=0;dropped<NDROP;dropped++){
                rpt = llpop();
                if (!rpt){
                    break;
                }
                to_free[dropped] = rpt;
            }
        }
        num_queued = ll_nqueued;
#ifdef USE_SPINLOCK
        pthread_spin_unlock(&ll_spinlock);
        if (1 == num_queued){
            pthread_mutex_lock(&ll_mutex);
            pthread_cond_signal(&cond);
            pthread_mutex_unlock(&ll_mutex);
#ifdef DEBUG
            fprintf(stderr,"ll notify\n");
#endif
        }
#else
        pthread_cond_signal(&cond);
        pthread_mutex_unlock(&ll_mutex);
#endif
        if (dropped){
            fprintf(stderr,"dropped %d buffer(s)\n",dropped);
            while(--dropped){
                if (to_free[dropped]){
                    free(to_free[dropped]);
                }
            }
        }
        if (num_queued > global_numq){
            printf("ll+, now %d\n", num_queued);
        }else if (num_queued < global_numq){
            printf("ll-, now %d\n", num_queued);
        }else{
#ifdef DEBUG 
            printf("ll, now %d\n", num_queued);
#endif
        }
        global_numq = num_queued;
    }
}

static void *tcp_worker(void *arg)
{
	struct llist *curelem,*prev;
	int bytesleft,bytessent, index;
	struct timeval tv;
	struct timespec ts;
	struct timeval tp;
	fd_set writefds;
	int r = 0;

    arg=arg;

    while(!thread_exit) {
#ifdef DEBUG
        fprintf(stderr,"ll wait notification\n");
#endif
        pthread_mutex_lock(&ll_mutex);
        gettimeofday(&tp, NULL);
        ts.tv_sec  = tp.tv_sec+5;
        ts.tv_nsec = tp.tv_usec * 1000;
        r = pthread_cond_timedwait(&cond, &ll_mutex, &ts);
        if( thread_exit || (r == ETIMEDOUT) ) {
            pthread_mutex_unlock(&ll_mutex);
            if (r == ETIMEDOUT){
                fprintf(stderr,"worker cond timeout\n");
            }else{
                fprintf(stderr,"worker got exit signal\n");
            }
            exit_threads();
            pthread_exit(NULL);
        }
#ifdef USE_SPINLOCK
        pthread_mutex_unlock(&ll_mutex);
        pthread_spin_lock(&ll_spinlock);
#endif
        curelem = llpop();
#ifdef USE_SPINLOCK
        pthread_spin_unlock(&ll_spinlock);
#else
        pthread_mutex_unlock(&ll_mutex);
#endif
        while(!thread_exit && curelem) {
            bytesleft = curelem->len;
            index = 0;
            bytessent = 0;
            while(!thread_exit && bytesleft > 0) {
                bytessent = send(s,  &curelem->data[index], bytesleft, 0);
                if (bytessent == SOCKET_ERROR) {
                    if (EAGAIN  == errno || EWOULDBLOCK == errno){
                        FD_ZERO(&writefds);
                        FD_SET(s, &writefds);
                        tv.tv_sec = 1;
                        tv.tv_usec = 0;
                        r = select(s+1, NULL, &writefds, NULL, &tv);
#ifdef DEBUG
                        if (r)
                            fprintf(stderr,"SELECT %d\n",r);
#endif
/*                        if(r) {
                            if (bytessent == SOCKET_ERROR) {
                                free(curelem);
                                perror("worker socket error");
                                exit_threads();
                                pthread_exit(NULL);
                            }
                            }*/
                    }else{
                        free(curelem);
                        perror("worker socket error");
                        exit_threads();
                        pthread_exit(NULL);
                    }
                }else{
#ifdef DEBUG
                    fprintf(stderr,"s:%d \n",bytessent);
#endif
                    bytesleft -= bytessent;
                    index += bytessent;
                }
            }
            free(curelem);

#ifdef USE_SPINLOCK
            pthread_spin_lock(&ll_spinlock);
#else
            pthread_mutex_lock(&ll_mutex);
#endif

            curelem = llpop();

#ifdef USE_SPINLOCK
            pthread_spin_unlock(&ll_spinlock);
#else
            pthread_mutex_unlock(&ll_mutex);
#endif
        }
    }
  	exit_threads();
    pthread_exit(0);
}

static int set_gain_by_index(rtlsdr_dev_t *_dev, unsigned int index)
{
	int res = 0;
	int* gains;
	int count = rtlsdr_get_tuner_gains(_dev, NULL);

	if (count > 0 && (unsigned int)count > index) {
		gains = malloc(sizeof(int) * count);
		count = rtlsdr_get_tuner_gains(_dev, gains);

		res = rtlsdr_set_tuner_gain(_dev, gains[index]);

		free(gains);
	}

	return res;
}

#ifdef _WIN32
#define __attribute__(x)
#pragma pack(push, 1)
#endif
struct command{
	unsigned char cmd;
	unsigned int param;
}__attribute__((packed));
#ifdef _WIN32
#pragma pack(pop)
#endif
static void *command_worker(void *arg)
{
	int left, received = 0;
	fd_set readfds;
	struct command cmd;
	struct timeval tv;
	int r = 0;
	uint32_t tmp;

    arg = arg;

	while(1) {
		left=sizeof(cmd);
		while(left >0) {
			FD_ZERO(&readfds);
			FD_SET(s, &readfds);
			tv.tv_sec = 0;
			tv.tv_usec = 500;
			r = select(s+1, &readfds, NULL, NULL, &tv);
            if(thread_exit || do_exit){
                fprintf(stderr,"comm thread exit\n");
                exit_threads();
                pthread_exit(NULL);
            }
			if(r) {
				received = recv(s, (char*)&cmd+(sizeof(cmd)-left), left, 0);
				if(received == SOCKET_ERROR){
                    perror("comm recv socket error");
					exit_threads();
					pthread_exit(NULL);
				} else {
					left -= received;
				}
			} 
		}
		switch(cmd.cmd) {
		case 0x01:
			printf("set freq %d\n", ntohl(cmd.param));
			rtlsdr_set_center_freq(dev,ntohl(cmd.param));
			break;
		case 0x02:
			printf("set sample rate %d\n", ntohl(cmd.param));
			rtlsdr_set_sample_rate(dev, ntohl(cmd.param));
            llclear();
			break;
		case 0x03:
			printf("set gain mode %d\n", ntohl(cmd.param));
			rtlsdr_set_tuner_gain_mode(dev, ntohl(cmd.param));
			break;
		case 0x04:
			printf("set gain %d\n", ntohl(cmd.param));
			rtlsdr_set_tuner_gain(dev, ntohl(cmd.param));
			break;
		case 0x05:
			printf("set freq correction %d\n", ntohl(cmd.param));
			rtlsdr_set_freq_correction(dev, ntohl(cmd.param));
			break;
		case 0x06:
			tmp = ntohl(cmd.param);
			printf("set if stage %d gain %d\n", tmp >> 16, (short)(tmp & 0xffff));
			rtlsdr_set_tuner_if_gain(dev, tmp >> 16, (short)(tmp & 0xffff));
			break;
		case 0x07:
			printf("set test mode %d\n", ntohl(cmd.param));
			rtlsdr_set_testmode(dev, ntohl(cmd.param));
			break;
		case 0x08:
			printf("set agc mode %d\n", ntohl(cmd.param));
			rtlsdr_set_agc_mode(dev, ntohl(cmd.param));
			break;
		case 0x09:
			printf("set direct sampling %d\n", ntohl(cmd.param));
			rtlsdr_set_direct_sampling(dev, ntohl(cmd.param));
			break;
		case 0x0a:
			printf("set offset tuning %d\n", ntohl(cmd.param));
			rtlsdr_set_offset_tuning(dev, ntohl(cmd.param));
			break;
		case 0x0b:
			printf("set rtl xtal %d\n", ntohl(cmd.param));
			rtlsdr_set_xtal_freq(dev, ntohl(cmd.param), 0);
			break;
		case 0x0c:
			printf("set tuner xtal %d\n", ntohl(cmd.param));
			rtlsdr_set_xtal_freq(dev, 0, ntohl(cmd.param));
			break;
		case 0x0d:
			printf("set tuner gain by index %d\n", ntohl(cmd.param));
			set_gain_by_index(dev, ntohl(cmd.param));
			break;
		default:
			break;
		}
		cmd.cmd = 0xff;
	}
}

int main(int argc, char **argv)
{
	int r, opt, i;
	char* addr = "127.0.0.1";
	char* iface = NULL;
	int port = 1234;
	uint32_t frequency = 100000000, samp_rate = 2048000;
	struct sockaddr_in local, remote;
	uint32_t buf_num = 0;
	int dev_index = 0;
	int dev_given = 0;
	int gain = 0;
	int ppm_error = 0;
	struct llist *curelem,*prev;
	pthread_attr_t attr;
	struct timeval tv;
	struct linger ling = {1,0};
    int nodelay = 1;
	SOCKET listensocket;
	struct ifreq ifr;
	socklen_t rlen;
	fd_set readfds;
	u_long blockmode = 1;
	dongle_info_t dongle_info;
    struct timespec ts;
#ifdef _WIN32
	WSADATA wsd;
	i = WSAStartup(MAKEWORD(2,2), &wsd);
#else
	struct sigaction sigact, sigign;
#endif

	while ((opt = getopt(argc, argv, "a:i:p:f:g:s:b:n:d:l:P:")) != -1) {
		switch (opt) {
		case 'd':
			dev_index = verbose_device_search(optarg);
			dev_given = 1;
			break;
		case 'f':
			frequency = (uint32_t)atofs(optarg);
			break;
		case 'g':
			gain = (int)(atof(optarg) * 10); /* tenths of a dB */
			break;
		case 's':
			samp_rate = (uint32_t)atofs(optarg);
			break;
		case 'a':
			addr = optarg;
			break;
        case 'i':
            iface = optarg;
            break;
		case 'p':
			port = atoi(optarg);
			break;
		case 'b':
			buf_num = atoi(optarg);
			break;
		case 'n':
			llbuf_num = atoi(optarg);
			break;
		case 'l':
			buf_len = atoi(optarg);
		case 'P':
			ppm_error = atoi(optarg);
			break;
		default:
			usage();
			break;
		}
	}

	if (argc < optind)
		usage();

	if (!dev_given) {
		dev_index = verbose_device_search("0");
	}

	if (dev_index < 0) {
	    exit(1);
	}

	rtlsdr_open(&dev, (uint32_t)dev_index);
	if (NULL == dev) {
	fprintf(stderr, "Failed to open rtlsdr device #%d.\n", dev_index);
		exit(1);
	}

#ifndef _WIN32
	sigact.sa_handler = sighandler;
	sigemptyset(&sigact.sa_mask);
	sigact.sa_flags = 0;
	sigign.sa_handler = SIG_IGN;
	sigaction(SIGINT, &sigact, NULL);
	sigaction(SIGTERM, &sigact, NULL);
	sigaction(SIGQUIT, &sigact, NULL);
	sigaction(SIGPIPE, &sigign, NULL);
#else
	SetConsoleCtrlHandler( (PHANDLER_ROUTINE) sighandler, TRUE );
#endif

	/* Set the tuner error */
	verbose_ppm_set(dev, ppm_error);

	/* Set the sample rate */
	r = rtlsdr_set_sample_rate(dev, samp_rate);
	if (r < 0)
		fprintf(stderr, "WARNING: Failed to set sample rate.\n");

	/* Set the frequency */
	r = rtlsdr_set_center_freq(dev, frequency);
	if (r < 0)
		fprintf(stderr, "WARNING: Failed to set center freq.\n");
	else
		fprintf(stderr, "Tuned to %i Hz.\n", frequency);

	if (0 == gain) {
		 /* Enable automatic gain */
		r = rtlsdr_set_tuner_gain_mode(dev, 0);
		if (r < 0)
			fprintf(stderr, "WARNING: Failed to enable automatic gain.\n");
	} else {
		/* Enable manual gain */
		r = rtlsdr_set_tuner_gain_mode(dev, 1);
		if (r < 0)
			fprintf(stderr, "WARNING: Failed to enable manual gain.\n");

		/* Set the tuner gain */
		r = rtlsdr_set_tuner_gain(dev, gain);
		if (r < 0)
			fprintf(stderr, "WARNING: Failed to set tuner gain.\n");
		else
			fprintf(stderr, "Tuner gain set to %f dB.\n", gain/10.0);
	}

	/* Reset endpoint before we start reading from it (mandatory) */
	r = rtlsdr_reset_buffer(dev);
	if (r < 0)
		fprintf(stderr, "WARNING: Failed to reset buffers.\n");

#ifdef USE_SPINLOCK
	pthread_spin_init(&ll_spinlock, 0);
#endif
	pthread_mutex_init(&ll_mutex, NULL);
	pthread_cond_init(&cond, NULL);

	memset(&local,0,sizeof(local));
	local.sin_family = AF_INET;
	local.sin_port = htons(port);
	local.sin_addr.s_addr = inet_addr(addr);

	listensocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (iface) {
            ifr.ifr_addr.sa_family = AF_INET;
            strncpy(ifr.ifr_name, iface, IFNAMSIZ-1);
            if (-1 != ioctl(listensocket, SIOCGIFADDR, &ifr)) {
                    local.sin_addr.s_addr = ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr.s_addr;
            }
    }
	r = 1;
	setsockopt(listensocket, SOL_SOCKET, SO_REUSEADDR, (char *)&r, sizeof(int));
	setsockopt(listensocket, SOL_SOCKET, SO_LINGER, (char *)&ling, sizeof(ling));
	bind(listensocket,(struct sockaddr *)&local,sizeof(local));

#ifdef _WIN32
	ioctlsocket(listensocket, FIONBIO, &blockmode);
#else
	r = fcntl(listensocket, F_GETFL, 0);
	r = fcntl(listensocket, F_SETFL, r | O_NONBLOCK);
#endif

	while(!do_exit) {

        ll_buffers_tail = ll_buffers_head = NULL;

		printf("listening...\n");
		printf("Use the device argument 'rtl_tcp=%s:%d' in OsmoSDR "
		       "(gr-osmosdr) source\n"
		       "to receive samples in GRC and control "
		       "rtl_tcp parameters (frequency, gain, ...).\n",
		       inet_ntoa(local.sin_addr), port);
		listen(listensocket,1);

		while(1) {
			FD_ZERO(&readfds);
			FD_SET(listensocket, &readfds);
			tv.tv_sec = 1;
			tv.tv_usec = 0;
			r = select(listensocket+1, &readfds, NULL, NULL, &tv);
			if(do_exit) {
				goto out;
			} else if(r) {
				rlen = sizeof(remote);
				s = accept(listensocket,(struct sockaddr *)&remote, &rlen);
				break;
			}
		}

		setsockopt(s, SOL_SOCKET, SO_LINGER, (char *)&ling, sizeof(ling));

		r = setsockopt(s, IPPROTO_TCP, TCP_NODELAY, (char *)&nodelay, sizeof(nodelay));

        if (r){
            fprintf(stderr,"TCP_NODELAY is NOT set! %d\n",r);
        }


		printf("client accepted!\n");

		memset(&dongle_info, 0, sizeof(dongle_info));
		memcpy(&dongle_info.magic, "RTL0", 4);

		r = rtlsdr_get_tuner_type(dev);
		if (r >= 0)
			dongle_info.tuner_type = htonl(r);

		r = rtlsdr_get_tuner_gains(dev, NULL);
		if (r >= 0)
			dongle_info.tuner_gain_count = htonl(r);

		r = send(s, (const char *)&dongle_info, sizeof(dongle_info), 0);
		if (sizeof(dongle_info) != r)
			printf("failed to send dongle information\n");

        #ifdef _WIN32
        ioctlsocket(s, FIONBIO, &blockmode);
        #else
        r = fcntl(s, F_GETFL, 0);
        r = fcntl(s, F_SETFL, r | O_NONBLOCK);
        #endif

		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
		r = pthread_create(&tcp_worker_thread, &attr, tcp_worker, NULL);
		r = pthread_create(&command_thread, &attr, command_worker, NULL);
		pthread_attr_destroy(&attr);

		fprintf(stderr,"start to work...\n");
		r = rtlsdr_read_async(dev, rtlsdr_callback, NULL, buf_num, buf_len*1024);
		exit_threads();
        fprintf(stderr,"wait for threads...\n");
		
		closesocket(s);

        pthread_join(command_thread, NULL);
		fprintf(stderr,"command thread is dead..\n");
		
        pthread_join(tcp_worker_thread, NULL);
 		fprintf(stderr,"worker thread is dead..\n");

		ts.tv_sec  = 0;
		ts.tv_nsec = 0;
		r = pthread_cond_timedwait(&cond, &ll_mutex, &ts);
		pthread_mutex_unlock(&ll_mutex);

		fprintf(stderr,"all threads are dead..\n");

		llclear();
		thread_exit = 0;
		global_numq = 0;
	}

out:
	rtlsdr_close(dev);
	closesocket(listensocket);
	closesocket(s);
#ifdef _WIN32
	WSACleanup();
#endif
	printf("bye!\n");
	return r >= 0 ? r : -r;
}
